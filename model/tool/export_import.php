<?php

static $registry = null;

// Error Handler
function error_handler_for_export_import($errno, $errstr, $errfile, $errline) {
	global $registry;
	
	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$errors = "Notice";
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$errors = "Warning";
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$errors = "Fatal Error";
			break;
		default:
			$errors = "Unknown";
			break;
	}
	
	$config = $registry->get('config');
	$url = $registry->get('url');
	$request = $registry->get('request');
	$session = $registry->get('session');
	$log = $registry->get('log');
	
	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $errors . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	}

	if (($errors=='Warning') || ($errors=='Unknown')) {
		return true;
	}

	if (($errors != "Fatal Error") && isset($request->get['route']) && ($request->get['route']!='tool/export_import/download'))  {
		if ($config->get('config_error_display')) {
			echo '<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
		}
	} else {
		$session->data['export_import_error'] = array( 'errstr'=>$errstr, 'errno'=>$errno, 'errfile'=>$errfile, 'errline'=>$errline );
		$token = $request->get['token'];
		$link = $url->link( 'tool/export_import', 'token='.$token, 'SSL' );
		header('Status: ' . 302);
		header('Location: ' . str_replace(array('&amp;', "\n", "\r"), array('&', '', ''), $link));
		exit();
	}

	return true;
}


function fatal_error_shutdown_handler_for_export_import()
{
	$last_error = error_get_last();
	if ($last_error['type'] === E_ERROR) {
		// fatal error
		error_handler_for_export_import(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
	}
}


class ModelToolExportImport extends Model {

	private $error = array();

	protected function clean( &$str, $allowBlanks=false ) {
		$result = "";
		$n = strlen( $str );
		for ($m=0; $m<$n; $m++) {
			$ch = substr( $str, $m, 1 );
			if (($ch==" ") && (!$allowBlanks) || ($ch=="\n") || ($ch=="\r") || ($ch=="\t") || ($ch=="\0") || ($ch=="\x0B")) {
				continue;
			}
			$result .= $ch;
		}
		return $result;
	}


	protected function rus2translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
			
			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		$string = strtr($string, $converter);
		$string = strtolower($string);
		$string = preg_replace('~[^-a-z0-9_]+~u', '-', $string);
		$string = trim($string, "-");
		return $string;
	}


	protected function multiquery( $sql ) {
		foreach (explode(";\n", $sql) as $sql) {
			$sql = trim($sql);
			if ($sql) {
				$this->db->query($sql);
			}
		}
	}


	protected function startsWith( $haystack, $needle ) {
		if (strlen( $haystack ) < strlen( $needle )) {
			return false;
		}
		return (substr( $haystack, 0, strlen($needle) ) == $needle);
	}

	protected function endsWith( $haystack, $needle ) {
		if (strlen( $haystack ) < strlen( $needle )) {
			return false;
		}
		return (substr( $haystack, strlen($haystack)-strlen($needle), strlen($needle) ) == $needle);
	}


	protected function getDefaultLanguageId() {
		$code = $this->config->get('config_language');
		$sql = "SELECT language_id FROM `".DB_PREFIX."language` WHERE code = '$code'";
		$result = $this->db->query( $sql );
		$language_id = 1;
		if ($result->rows) {
			foreach ($result->rows as $row) {
				$language_id = $row['language_id'];
				break;
			}
		}
		return $language_id;
	}


	protected function getLanguages() {
		$query = $this->db->query( "SELECT * FROM `".DB_PREFIX."language` WHERE `status`=1 ORDER BY `code`" );
		return $query->rows;
	}


	protected function getDefaultWeightUnit() {
		$weight_class_id = $this->config->get( 'config_weight_class_id' );
		$language_id = $this->getDefaultLanguageId();
		$sql = "SELECT unit FROM `".DB_PREFIX."weight_class_description` WHERE language_id='".(int)$language_id."'";
		$query = $this->db->query( $sql );
		if ($query->num_rows > 0) {
			return $query->row['unit'];
		}
		$sql = "SELECT language_id FROM `".DB_PREFIX."language` WHERE code = 'en'";
		$query = $this->db->query( $sql );
		if ($query->num_rows > 0) {
			$language_id = $query->row['language_id'];
			$sql = "SELECT unit FROM `".DB_PREFIX."weight_class_description` WHERE language_id='".(int)$language_id."'";
			$query = $this->db->query( $sql );
			if ($query->num_rows > 0) {
				return $query->row['unit'];
			}
		}
		return 'kg';
	}


	protected function getDefaultMeasurementUnit() {
		$length_class_id = $this->config->get( 'config_length_class_id' );
		$language_id = $this->getDefaultLanguageId();
		$sql = "SELECT unit FROM `".DB_PREFIX."length_class_description` WHERE language_id='".(int)$language_id."'";
		$query = $this->db->query( $sql );
		if ($query->num_rows > 0) {
			return $query->row['unit'];
		}
		$sql = "SELECT language_id FROM `".DB_PREFIX."language` WHERE code = 'en'";
		$query = $this->db->query( $sql );
		if ($query->num_rows > 0) {
			$language_id = $query->row['language_id'];
			$sql = "SELECT unit FROM `".DB_PREFIX."length_class_description` WHERE language_id='".(int)$language_id."'";
			$query = $this->db->query( $sql );
			if ($query->num_rows > 0) {
				return $query->row['unit'];
			}
		}
		return 'cm';
	}


	protected function clearCache() {
		$this->cache->delete('*');
	}
	
	protected function convert_coder($string) {
		if (mb_detect_encoding($string, 'UTF-8', true)==true)
		return $string;
		else
		return iconv('windows-1251', 'UTF-8', $string);
	}


	public function upload( $filename , $file_info) {
		$handle = fopen($filename, "r");
		$array_line_full = array();
		while (($line = fgetcsv($handle, 0, ";")) !== FALSE) { 
			$array_line_full[] = $line;
		}
		fclose($handle);
		
		$ftp = 0;
		$add_count = 0;
		$update_count = 0;
		$filename_for_db = $file_info['name'];
 
		$skladi_array = array();
		$products = array();
        
		$i = 0;
		$price_type = 0;
		foreach ($array_line_full as $one_line_csv)
		{
			if ($i == 0) { // Информация о прайсе
				$date_price = str_replace('dateTime:','',$this->convert_coder($one_line_csv[0]));
			} 
				else
			if ($i == 1) // Заказы поставщикам
			{
				$orders_text = explode(':',$this->convert_coder($one_line_csv[0]));
				$orders_array = explode(',',$this->convert_coder($orders_text[1]));
			} else
			if ($i == 2) // Склады
			{
				$skladis_text = explode(':',$this->convert_coder($one_line_csv[0]));
				$skladi_array = explode(',',$this->convert_coder($skladis_text[1]));
			}
				else 
			if ($i == 3) 
			{
				$zagolovki_array = $one_line_csv;
			} // Заголовки
			else // Все данные
				{
					$products[] = $one_line_csv;
				}
			$i++;
		}
		
		$orders_sootvetstvie = array();
		$j = 0;
		
		foreach ($orders_array as $one_order_array) {
			$explode_order = explode('|',$one_order_array);
			$query_order = $this->db->query( "SELECT id_zakaz FROM `".DB_PREFIX."table_order` WHERE name='".$this->db->escape($explode_order[0])."' AND factory='".$this->db->escape($explode_order[1])."' AND `date`='".$this->db->escape($explode_order[2])."' LIMIT 0,1" );
			if (isset($query_order->row['id_zakaz'])&&$query_order->row['id_zakaz'])
			{
				$current_order_id = $query_order->row['id_zakaz'];
			}
			else
			{
				$add_order = $this->db->query( "INSERT INTO `".DB_PREFIX."table_order` SET name='".$this->db->escape($explode_order[0])."',factory='".$this->db->escape($explode_order[1])."',`date`='".$this->db->escape($explode_order[2])."'" );
				$current_order_id = mysql_insert_id();
			}
				
			$orders_sootvetstvie[$j] = $current_order_id;
			$j++;
		}
		
		$skladis_sootvetstvie = array();
		$j = 0;
		
		// Склады
		foreach ($skladi_array as $one_sklad)
		{
			$query_skladi = $this->db->query( "SELECT skladi_id FROM `".DB_PREFIX."skladi` WHERE name='".$this->db->escape($one_sklad)."' LIMIT 0,1" );
			if (isset($query_skladi->row['skladi_id'])&&$query_skladi->row['skladi_id'])
			{
				$current_skladi = $query_skladi->row['skladi_id'];
			}
			else
			{
				$add_skladi = $this->db->query( "INSERT INTO `".DB_PREFIX."skladi` SET name='".$this->db->escape($one_sklad)."', town_id='0', sort_order='0'" );
				$current_skladi = mysql_insert_id();
			}
				
			$skladis_sootvetstvie[$j] = $current_skladi;
			$j++;
		}
		
		$names_array = array();
		
		$default_language_id = $this->getDefaultLanguageId();
		$storeId = 0;
		$stock_status_id_vnalich = 7;
		
		if ($products)
		foreach ($products as $one_product)
		{
			$new_model = $this->rus2translit($this->convert_coder($one_product[0]));
			$code = $this->convert_coder($one_product[0]);
			if (isset($one_product[1])) {
			$categories = explode('|',$this->convert_coder($one_product[1]));
			$name = $this->convert_coder($one_product[2]);
			$names_array[$name] = '';
			$sku = $this->convert_coder($one_product[3]);
			$filter_1 = $this->convert_coder($one_product[4]);
			$filter_2 = $this->convert_coder($one_product[5]);
			$filter_3 = $this->convert_coder($one_product[6]);
			$filter_4 = $this->convert_coder($one_product[7]);
			$filter_5 = $this->convert_coder($one_product[8]);
			$filter_6 = $this->convert_coder($one_product[9]);
			$oem = $this->convert_coder($one_product[10]);
			$manufacturer = $this->convert_coder($one_product[11]);
			$attributes_string = $this->convert_coder($one_product[12]);
			$order = explode(',',$this->convert_coder($one_product[13]));
			$price_1 = $this->convert_coder($one_product[14]);
			$price_2 = $this->convert_coder($one_product[15]);
			$price_3 = $this->convert_coder($one_product[16]);
			$skladis = explode(',',$this->convert_coder($one_product[17]));
			$description = $this->convert_coder($one_product[20]);
			$main_count = 0;
			foreach ($skladis as $one_sklad)
				$main_count+=$one_sklad;
			
			
			$query = $this->db->query( "SELECT COUNT(p.product_id) AS pid FROM `".DB_PREFIX."product` p WHERE p.model='".$this->db->escape($new_model)."'" );
			$count_products = $query->row['pid'];
			
			if ($count_products == 0)
			{
				$add_count++;
				// Создаем новый товар
				if (isset($one_product[19]))
					$analogi = $this->convert_coder($one_product[19]);
				else
					$analogi = '';
				
				// Изображение
				$images_array = array();
				//$all_images = str_replace(' ','%20',$this->convert_coder($one_product[18]));
			
				$images_temp_array = explode(",",$one_product[18]);
				
				$structure = DIR_IMAGE."data/";
				$structure_4db = "data/";

				if (!is_dir($structure)) {
					if (!mkdir($structure, 0777, true)) {
						//echo 'Not create directory...';
					}
				}
				
				/*$jj = 0;
				$also_images = array();
				foreach ($images_temp_array as $one_image) {
					$one_image = trim($one_image,",");
					$one_image = $one_image.'.JPG';
					if ($one_image!='.JPG')
					$images_array[] = $one_image;
				}

				foreach ($images_array as $one_image) { 
					$path = $structure.$new_model."_".$jj.".JPG";
					$fp = fopen($path, 'w');

					$ch = curl_init($one_image);
					curl_setopt($ch, CURLOPT_FILE, $fp);

					$data = curl_exec($ch);

					curl_close($ch);
					fclose($fp);
					
					if ($jj==0)
						$main_image = $structure_4db.$new_model."_".$jj.".JPG";
					else
						$also_images[] = $structure_4db.$new_model."_".$jj.".JPG";
					
					$jj++;
				}*/
				// Новые изображения
				$also_images = array();
				$jj = 0;
				foreach ($images_temp_array as $one_image) {
					$mini_image_array = explode('|',$one_image);
					if ($jj==0)
						$main_image = $structure_4db.$mini_image_array[0];
					else
						$also_images[] = array(
							'image'	=>	$structure_4db.$mini_image_array[0],
							'description'	=>	$mini_image_array[1]
						);
					$jj++;
				}
				
				// Категория
				$first_category = 1;
				$to_categories = array();
				$previous_category = '';
				foreach ($categories as $category) {
					if (isset($current_category))
						$previous_category = $current_category;
					$query_category = $this->db->query( "SELECT cd.category_id FROM `".DB_PREFIX."category_description` cd WHERE cd.name='".$this->db->escape($category)."' AND language_id='".$default_language_id."' LIMIT 0,1" );
					if (isset($query_category->row['category_id'])&&$query_category->row['category_id'])
					{
						$current_category = $query_category->row['category_id'];
					}
					else
					{
						if ($first_category==1)
							$add_category = $this->db->query( "INSERT INTO `".DB_PREFIX."category` SET parent_id='0', top='1', `column`='1', sort_order='0', `status`='1', date_added=NOW(), date_modified=NOW()" );
						else
							$add_category = $this->db->query( "INSERT INTO `".DB_PREFIX."category` SET parent_id='".$previous_category."', top='1', `column`='1', sort_order='0', `status`='1', date_added=NOW(), date_modified=NOW()" );
						$current_category = mysql_insert_id();
						$add_category_2 = $this->db->query( "INSERT INTO `".DB_PREFIX."category_description` SET category_id='".$current_category."', language_id='".$default_language_id."', name='".$this->db->escape($category)."', meta_title='".$this->db->escape($category)."'" );
						$add_category_3 = $this->db->query( "INSERT INTO `".DB_PREFIX."category_to_store` SET category_id='".$current_category."', store_id='".$storeId."'" );
						$add_category_4 = $this->db->query( "INSERT INTO `".DB_PREFIX."category_to_layout` SET category_id='".$current_category."', store_id='".$storeId."', layout_id='0'" );
						if ($first_category==1)
							$add_category_5 = $this->db->query( "INSERT INTO `".DB_PREFIX."category_path` SET category_id='".$current_category."', path_id='".$current_category."', level='0'" );
						else
						{
							$add_category_5 = $this->db->query( "INSERT INTO `".DB_PREFIX."category_path` SET category_id='".$current_category."', path_id='".$current_category."', level='1'" );
							$add_category_5 = $this->db->query( "INSERT INTO `".DB_PREFIX."category_path` SET category_id='".$current_category."', path_id='".$previous_category."', level='0'" );
						}
						
					}
					$to_categories[] = $current_category;
					$first_category = 0;
				}
				
				// Производитель
				$query_manufacturer = $this->db->query( "SELECT m.manufacturer_id FROM `".DB_PREFIX."manufacturer` m WHERE m.name='".$this->db->escape($manufacturer)."' LIMIT 0,1" );
				if (isset($query_manufacturer->row['manufacturer_id'])&&$query_manufacturer->row['manufacturer_id'])
				{
					$current_manufacturer = $query_manufacturer->row['manufacturer_id'];
				}
				else
				{
					$add_manufacturer = $this->db->query( "INSERT INTO `".DB_PREFIX."manufacturer` SET name='".$this->db->escape($manufacturer)."', sort_order='0'" );
					$current_manufacturer = mysql_insert_id();
					$add_category_3 = $this->db->query( "INSERT INTO `".DB_PREFIX."manufacturer_to_store` SET manufacturer_id='".$current_manufacturer."', store_id='".$storeId."'" );
				}
				
				$add_product = $this->db->query( "INSERT INTO `".DB_PREFIX."product` SET image='".$main_image."', model='".$this->db->escape($new_model)."', sku='".$this->db->escape($sku)."', quantity='".(int)$main_count."', `stock_status_id`='".$stock_status_id_vnalich."', manufacturer_id='".$current_manufacturer."', `price`='".$price_2."', date_available=NOW(), `status`='1', date_added=NOW(), date_modified=NOW(), filter_options='".$attributes_string."', filter_1='".$filter_1."', filter_2='".$filter_2."', filter_3='".$filter_3."', filter_4='".$filter_4."', filter_5='".$filter_5."', filter_6='".$filter_6."', main_code='".$code."', oem='".$oem."', price_2='".$price_1."', price_3='".$price_3."', analog='".$analogi."' " );
				
				$current_product = mysql_insert_id();	
				$add_product_2 = $this->db->query( "INSERT INTO `".DB_PREFIX."product_description` SET product_id='".$current_product."', language_id='".$default_language_id."', name='".$this->db->escape($name)."', description='".$this->db->escape($description)."', meta_title='".$this->db->escape($name)."'" );
				foreach ($to_categories as $one_mini_category)
					$add_product_3 = $this->db->query( "INSERT INTO `".DB_PREFIX."product_to_category` SET product_id='".$current_product."', category_id='".$one_mini_category."'" );
				$add_product_4 = $this->db->query( "INSERT INTO `".DB_PREFIX."product_to_layout` SET product_id='".$current_product."', store_id='".$storeId."', layout_id='0'" );
				$add_product_5 = $this->db->query( "INSERT INTO `".DB_PREFIX."product_to_store` SET product_id='".$current_product."', store_id='".$storeId."'" );
				$xx = 0;
				if ($also_images)
				foreach ($also_images as $also_image)
				{
					$add_product_6 = $this->db->query( "INSERT INTO `".DB_PREFIX."product_image` SET product_id='".$current_product."', image='".$also_image['image']."',image_description='".$also_image['description']."',sort_order='".$xx."'" );
					$xx++;
				}
				
				// Атрибуты
				$attributes = array();
				if ($attributes_string)
				{
					$attributes_temp = explode(',',$attributes_string);
					foreach ($attributes_temp as $one_attribute_temp)
					{
						$explode_attributes = explode('|',$one_attribute_temp);
						$attributes[] = array(
							'group'	=>	$explode_attributes[0],
							'name'	=>	$explode_attributes[1],
							'value'	=>	$explode_attributes[2]
						);
					}
				}
				
				foreach ($attributes as $one_attribute)
				{
					$query_attribute_group = $this->db->query( "SELECT agd.attribute_group_id FROM `".DB_PREFIX."attribute_group_description` agd WHERE agd.name='".$this->db->escape($one_attribute['group'])."' LIMIT 0,1" );
					if (isset($query_attribute_group->row['attribute_group_id'])&&$query_attribute_group->row['attribute_group_id'])
					{
						$current_attribute_group_id = $query_attribute_group->row['attribute_group_id'];
					}
					else
					{
						$add_attribute_group_id = $this->db->query( "INSERT INTO `".DB_PREFIX."attribute_group` SET sort_order='0'" );
						$current_attribute_group_id = mysql_insert_id();
						$add_attribute_3 = $this->db->query( "INSERT INTO `".DB_PREFIX."attribute_group_description` SET attribute_group_id='".$current_attribute_group_id."',language_id='".$default_language_id."',name='".$this->db->escape($one_attribute['group'])."'" );
					}
					$query_attribute = $this->db->query( "SELECT ad.attribute_id FROM `".DB_PREFIX."attribute_description` ad LEFT JOIN attribute a ON a.attribute_id=ad.attribute_id WHERE ad.name='".$this->db->escape($one_attribute['name'])."' AND a.attribute_group_id='".$current_attribute_group_id."' LIMIT 0,1" );
					if (isset($query_attribute->row['attribute_id'])&&$query_attribute->row['attribute_id'])
					{
						$current_attribute_id = $query_attribute->row['attribute_id'];
					}
					else
					{
						$add_attribute_id = $this->db->query( "INSERT INTO `".DB_PREFIX."attribute` SET attribute_group_id='".$current_attribute_group_id."', sort_order='0'" );
						$current_attribute_id = mysql_insert_id();
						$add_attribute_4 = $this->db->query( "INSERT INTO `".DB_PREFIX."attribute_description` SET attribute_id='".$current_attribute_id."',language_id='".$default_language_id."',name='".$this->db->escape($one_attribute['name'])."'" );
					}
					$add_attribute_product = $this->db->query( "INSERT INTO `".DB_PREFIX."product_attribute` SET product_id='".$current_product."',language_id='".$default_language_id."',attribute_id='".$current_attribute_id."',text='".$one_attribute['value']."'" );
				}
			
			}
			else 
			{
				$update_count++;
				// Обновляем цены
				$query = $this->db->query( "SELECT product_id FROM `".DB_PREFIX."product` WHERE model='".$this->db->escape($new_model)."' LIMIT 0,1" );
				$current_product = $query->row['product_id'];
			}
			
			// Обновляем количество на складах
			$j = 0;
			$add_manufacturer = $this->db->query( "DELETE FROM `".DB_PREFIX."skladi_to_products` WHERE product_id='".(int)$current_product."'" );
			
			foreach ($skladis as $one_sklad)
			{
				$add_manufacturer = $this->db->query( "INSERT INTO `".DB_PREFIX."skladi_to_products` SET product_id='".(int)$current_product."', sklad_id='".$skladis_sootvetstvie[$j]."', quantity='".$one_sklad."'" );
				$j++;
			}
			
			$j = 0;
			// Обновляем количество для поставщиков
			foreach ($order as $one_attr)
			{
				$delete_order_product = $this->db->query( "DELETE FROM `".DB_PREFIX."table_order_product` WHERE id_product='".(int)$current_product."' AND id_zakaz='".$orders_sootvetstvie[$j]."'" );
				if ($one_attr!=0)
					$add_order_product = $this->db->query( "INSERT INTO `".DB_PREFIX."table_order_product` SET id_product='".(int)$current_product."', id_zakaz='".$orders_sootvetstvie[$j]."', count='".$one_attr."'" );
				$j++;
			}
			}
			
		}
		
		foreach ($names_array as $current_name=>$one_name) {
			$related_ids = $this->db->query( "SELECT product_id FROM `".DB_PREFIX."product_description` WHERE name='".$this->db->escape($current_name)."' AND language_id='".$default_language_id."'" );
			if (count($related_ids->rows)>1)
			foreach ($related_ids->rows as $first_related_id)
			{
				$this->db->query( "DELETE FROM `".DB_PREFIX."product_related` WHERE product_id='".(int)$first_related_id['product_id']."'" );
				$this->db->query( "DELETE FROM `".DB_PREFIX."product_related` WHERE related_id='".(int)$first_related_id['product_id']."'" );
			}
			foreach ($related_ids->rows as $first_related_id)
			{
				foreach ($related_ids->rows as $second_related_id)
				{
					if ($first_related_id!=$second_related_id)
					{
						$this->db->query( "INSERT INTO `".DB_PREFIX."product_related` SET product_id='".$first_related_id['product_id']."', related_id='".$second_related_id['product_id']."'" );
					}
				}
			}
		}
		
		$price_history = $this->db->query( "INSERT INTO `".DB_PREFIX."price_history` SET date_price='".$date_price."', date_load=NOW(), add_count='".$add_count."', update_count='".$update_count."', ftp='".$ftp."',filename='".$filename_for_db."'");
		
		return true;
		
	}



	protected function clearSpreadsheetCache() {
		$files = glob(DIR_CACHE . 'Spreadsheet_Excel_Writer' . '*');
		
		if ($files) {
			foreach ($files as $file) {
				if (file_exists($file)) {
					@unlink($file);
					clearstatcache();
				}
			}
		}
	}
   

	public function getMaxProductId() {
		$query = $this->db->query( "SELECT MAX(product_id) as max_product_id FROM `".DB_PREFIX."product`" );
		if (isset($query->row['max_product_id'])) {
			$max_id = $query->row['max_product_id'];
		} else {
			$max_id = 0;
		}
		return $max_id;
	}


	public function getMinProductId() {
		$query = $this->db->query( "SELECT MIN(product_id) as min_product_id FROM `".DB_PREFIX."product`" );
		if (isset($query->row['min_product_id'])) {
			$min_id = $query->row['min_product_id'];
		} else {
			$min_id = 0;
		}
		return $min_id;
	}


	public function getCountProduct() {
		$query = $this->db->query( "SELECT COUNT(product_id) as count_product FROM `".DB_PREFIX."product`" );
		if (isset($query->row['count_product'])) {
			$count = $query->row['count_product'];
		} else {
			$count = 0;
		}
		return $count;
	}  

 
	public function getMaxCategoryId() {
		$query = $this->db->query( "SELECT MAX(category_id) as max_category_id FROM `".DB_PREFIX."category`" );
		if (isset($query->row['max_category_id'])) {
			$max_id = $query->row['max_category_id'];
		} else {
			$max_id = 0;
		}
		return $max_id;
	}


	public function getMinCategoryId() {
		$query = $this->db->query( "SELECT MIN(category_id) as min_category_id FROM `".DB_PREFIX."category`" );
		if (isset($query->row['min_category_id'])) {
			$min_id = $query->row['min_category_id'];
		} else {
			$min_id = 0;
		}
		return $min_id;
	}


	public function getCountCategory() {
		$query = $this->db->query( "SELECT COUNT(category_id) as count_category FROM `".DB_PREFIX."category`" );
		if (isset($query->row['count_category'])) {
			$count = $query->row['count_category'];
		} else {
			$count = 0;
		}
		return $count;
	}  

	protected function curl_get_contents($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}


	public function getNotifications() {
		$language_code = $this->config->get( 'config_admin_language' );
		$result = $this->curl_get_contents("http://www.mhccorp.com/index.php?route=information/message&type=tool_export_import_2_26&language_code=$language_code");
		if (stripos($result,'<html') !== false) {
			return '';
		}
		return $result;
	}


	public function getOptionNameCounts() {
		$default_language_id = $this->getDefaultLanguageId();
		$sql  = "SELECT `name`, COUNT(option_id) AS `count` FROM `".DB_PREFIX."option_description` ";
		$sql .= "WHERE language_id='".(int)$default_language_id."' ";
		$sql .= "GROUP BY `name`";
		$query = $this->db->query( $sql );
		return $query->rows;
	}


	public function getOptionValueNameCounts() {
		$default_language_id = $this->getDefaultLanguageId();
		$sql  = "SELECT option_id, `name`, COUNT(option_value_id) AS `count` FROM `".DB_PREFIX."option_value_description` ";
		$sql .= "WHERE language_id='".(int)$default_language_id."' ";
		$sql .= "GROUP BY option_id, `name`";
		$query = $this->db->query( $sql );
		return $query->rows;
	}


	public function getAttributeGroupNameCounts() {
		$default_language_id = $this->getDefaultLanguageId();
		$sql  = "SELECT `name`, COUNT(attribute_group_id) AS `count` FROM `".DB_PREFIX."attribute_group_description` ";
		$sql .= "WHERE language_id='".(int)$default_language_id."' ";
		$sql .= "GROUP BY `name`";
		$query = $this->db->query( $sql );
		return $query->rows;
	}


	public function getAttributeNameCounts() {
		$default_language_id = $this->getDefaultLanguageId();
		$sql  = "SELECT ag.attribute_group_id, ad.`name`, COUNT(ad.attribute_id) AS `count` FROM `".DB_PREFIX."attribute_description` ad ";
		$sql .= "INNER JOIN `".DB_PREFIX."attribute` a ON a.attribute_id=ad.attribute_id ";
		$sql .= "INNER JOIN `".DB_PREFIX."attribute_group` ag ON ag.attribute_group_id=a.attribute_group_id ";
		$sql .= "WHERE ad.language_id='".(int)$default_language_id."' ";
		$sql .= "GROUP BY ag.attribute_group_id, ad.`name`";
		$query = $this->db->query( $sql );
		return $query->rows;
	}
}
?>