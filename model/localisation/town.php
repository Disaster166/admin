<?php
class ModelLocalisationTown extends Model {
	public function addtown($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "town SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int)$data['country_id'] . "'");

		$this->cache->delete('town');
	}

	public function edittown($town_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "town SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int)$data['country_id'] . "' WHERE town_id = '" . (int)$town_id . "'");

		$this->cache->delete('town');
	}

	public function deletetown($town_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "town WHERE town_id = '" . (int)$town_id . "'");

		$this->cache->delete('town');
	}

	public function gettown($town_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "town WHERE town_id = '" . (int)$town_id . "'");

		return $query->row;
	}

	public function gettowns($data = array()) {
		$sql = "SELECT *, z.name, c.name AS country FROM " . DB_PREFIX . "town z LEFT JOIN " . DB_PREFIX . "country c ON (z.country_id = c.country_id)";

		$sort_data = array(
			'c.name',
			'z.name',
			'z.code'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY c.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function gettownsByCountryId($country_id) {
		$town_data = $this->cache->get('town.' . (int)$country_id);

		if (!$town_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "town WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

			$town_data = $query->rows;

			$this->cache->set('town.' . (int)$country_id, $town_data);
		}

		return $town_data;
	}

	public function getTotaltowns() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "town");

		return $query->row['total'];
	}

	public function getTotaltownsByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "town WHERE country_id = '" . (int)$country_id . "'");

		return $query->row['total'];
	}
}