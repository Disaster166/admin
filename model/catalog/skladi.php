<?php
class ModelCatalogSkladi extends Model {
	public function addskladi($data) {
		$this->event->trigger('pre.admin.skladi.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "skladi SET name = '" . $this->db->escape($data['name']) . "', address = '" . $this->db->escape($data['address']) . "', phone = '" . $this->db->escape($data['phone']) . "', worktime = '" . $this->db->escape($data['worktime']) . "', `town_id` = '" . (int)$data['town_id'] . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$skladi_id = $this->db->getLastId();

		$this->cache->delete('skladi');

		$this->event->trigger('post.admin.skladi.add', $skladi_id);

		return $skladi_id;
	}

	public function editskladi($skladi_id, $data) {
		$this->event->trigger('pre.admin.skladi.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "skladi SET name = '" . $this->db->escape($data['name']) . "', address = '" . $this->db->escape($data['address']) . "', phone = '" . $this->db->escape($data['phone']) . "', worktime = '" . $this->db->escape($data['worktime']) . "', `town_id` = '" . (int)$data['town_id'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE skladi_id = '" . (int)$skladi_id . "'");

		$this->cache->delete('skladi');

		$this->event->trigger('post.admin.skladi.edit', $skladi_id);
	}

	public function deleteskladi($skladi_id) {
		$this->event->trigger('pre.admin.skladi.delete', $skladi_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "skladi WHERE skladi_id = '" . (int)$skladi_id . "'");

		$this->cache->delete('skladi');

		$this->event->trigger('post.admin.skladi.delete', $skladi_id);
	}

	public function getskladi($skladi_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "skladi c  WHERE c.skladi_id = '" . (int)$skladi_id . "'");

		return $query->row;
	}

	public function getskladicount($product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "skladi_to_products stp LEFT JOIN " . DB_PREFIX . "skladi s ON s.skladi_id=stp.sklad_id WHERE stp.product_id = '" . (int)$product_id . "'");
		return $query->rows;
	}

	public function getSkladis($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "skladi WHERE 1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY skladi_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSkladis() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "skladi");

		return $query->row['total'];
	}
}
