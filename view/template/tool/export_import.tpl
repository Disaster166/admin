<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
	<div class="container-fluid">
	  <div class="pull-right">
		<a href="<?php echo $back; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	  </div>
	  <h1><?php echo $heading_title; ?></h1>
	  <ul class="breadcrumb">
	    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	  </ul>
	</div>
  </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>
	<?php } ?>
	<?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>
	<?php } ?>
	<div class="panel panel-default">
	  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-exchange"></i> <?php echo $heading_title; ?></h3>
      </div>
	  <div class="panel-body">
		<div class="tab-content" style='overflow:hidden;'>
			<form action="<?php echo $import; ?>" method="post" enctype="multipart/form-data" id="import" class="form-horizontal">
			  <div class="alert alert-info"><?php echo $entry_import; ?></div>	
			  <div class="form-group">
				<label class="col-sm-3 control-label"><?php echo $entry_upload; ?></label>
				<div class="col-sm-9">
				  <input type="file" name="upload" id="upload" />
				</div>
			  </div>
			  <div class="pull-right"><a onclick="uploadData();" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo $button_import; ?></a></div>
			</form>
		  
		</div>
		<?php if ($history) { ?>
		<div class="table-responsive" style='clear:both;margin-top: 20px;'>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
					<td>Имя файла</td>
					<td>Добавленных товаров</td>
					<td>Обновленных товаров</td>
					<td>Дата прайса</td>
					<td>Дата загрузки прайса</td>
					<td>Загрузка из FTP</td>
                </tr>
              </thead>
              <tbody>
				<?php foreach ($history as $history_element) { ?>
				<tr>
					<td><?php echo $history_element['filename']; ?></td>
					<td><?php echo $history_element['add_count']; ?></td>
					<td><?php echo $history_element['update_count']; ?></td>
					<td><?php echo $history_element['date_price']; ?></td>
					<td><?php echo $history_element['date_load']; ?></td>
					<td><?php echo $history_element['ftp']; ?></td>
				</tr>
				<?php } ?>
			  </tbody>
			</table>
		</div>
		<?php } ?>
	  </div>
	</div>
  </div>
<script type="text/javascript"><!--

function checkFileSize(id) {
	var input, file, file_size;

	if (!window.FileReader) {
		return true;
	}

	input = document.getElementById(id);
	if (!input) {
		return true;
	}
	else if (!input.files) {
		return true;
	}
	else if (!input.files[0]) {
		alert( "<?php echo $error_select_file; ?>" );
		return false;
	}
	else {
		file = input.files[0];
		file_size = file.size;
		<?php if (!empty($post_max_size)) { ?>
		post_max_size = <?php echo $post_max_size; ?>;
		if (file_size > post_max_size) {
			alert( "<?php echo $error_post_max_size; ?>" );
			return false;
		}
		<?php } ?>
		<?php if (!empty($upload_max_filesize)) { ?>
		upload_max_filesize = <?php echo $upload_max_filesize; ?>;
		if (file_size > upload_max_filesize) {
			alert( "<?php echo $error_upload_max_filesize; ?>" );
			return false;
		}
		<?php } ?>
		return true;
	}
}

function uploadData() {
	if (checkFileSize('upload')) {
		$('#import').submit();
	}
}

function isNumber(txt){ 
	var regExp=/^[\d]{1,}$/;
	return regExp.test(txt); 
}

function updateSettings() {
	$('#settings').submit();
}
//--></script></div>
<?php echo $footer; ?>
